<?

class auth_service
{
    public static function auth($raw, &$error, $send_activation = true)
    {
        $geo_config = config::all()->geo;

        if (!array_key_exists('city_id', $raw) && array_key_exists('city_title', $raw)) {
            $city_id = (int)geo_new_peer::city_id($raw['city_title'], $raw['country_title']);
        } elseif (!$raw['city_title'] && $raw['city_id'] ) {
            $city_id = $raw['city_id'];
        } else {
            $city_id = geo::get_geo()['city_id']?:0;
        }


        $data = array(
            'email'     => strtolower(str_replace(' ', '', $raw['email'])),
            'name'      => trim($raw['first_name'] ? $raw['first_name'] : $raw['name']),
            'photo' => ($raw['photo']) ? $raw['photo'] : '',
            'gender'    => $raw['gender'],
            'age'       => $raw['age'],
            'pwd'       => $raw['password'] ? md5($raw['password']) : md5(md5(microtime(true))),
            'ts'        => time(),
            'last_ts'   => time(),
            'one_more'  => 'field',
            'some_more' => 'smthng_here',
            'second_line' => 'data',
            'ip'        => sprintf("%u", ip2long($_SERVER['REMOTE_ADDR'])),
            'partner'   => (int)session::get('sign-partner'),
            'city_id'   => $city_id,
            'channel' => ($raw['channel']) ? $raw['channel'] : ''
        );

        //some more comment lines

        if ($raw['auto_login']) {
            session::set('user_from', 'auto');
            do::something();
        }

        $partner_ch = session::get('sign-partner-ch');
        partner_peer::track_reg_tries($data['partner']);

        if (!session::get('login-stats-p')) {
            session::set('login-stats-p', $data['partner']);
        }

        if ($raw['mm'] && (($mm_id = self::mm_id()) || ($mm_id = $raw['mm_id']))) {
            session::set('user_from','mm');
            $need_link_to_mm = false;
            if ($mm = social_mm_peer::get($mm_id)) {
                if ($mm['user_id'] && user_peer::get($mm['user_id'])) {
                    user::authorize($mm['user_id']);
                    $user = user_peer::get($mm['user_id']);
                } else {
                    $need_link_to_mm = true;
                }
            } else {
                $need_link_to_mm = true;
            }

            $u = user_peer::get_by('email', $data['email']);

            if ($need_link_to_mm && $u && $mm) {
                if(!user_peer::get($mm['user_id'])) {
                    social_mm_peer::insert(array(
                            'mm_id' => $raw['mm_id'],
                            'user_id' => $u['id'],
                            'ts' => $u['ts'],
                            'gender' => $u['gender'],
                            'email' => $u['email']
                        ));
                    } else {
                        social_mm_peer::save($raw['mm_id'],array('user_id' => $u['id']));
                    }

                helper_rtg::inc('mailru-linked');

                user::authorize($u['id']);
                $user = $u;
            }
            $activate = false;
        }

        if ($raw['google']) {
            unset($raw['birthday']);
            session::set('user_from', 'gg');
            if ($user_google = user_peer::get_by('email',$raw['email'])) {
                user::authorize($user_google['id']);
                $user = $user_google;
            }
            $activate = true;
        }

        if ($raw['fb']) {
            $activate = false;
            session::set('user_from','fb');
            $need_link_to_fb = false;

            if (!$fb_id = self::fb_id()) {
                $fb_id = $raw['fb_id'];
            }

            if (!$data['email']) {
                $activate = false;
                $data['email'] = $raw['username'] ? $raw['username'] : $fb_id;
                $data['email'] .= '@facebook.com';
            }

            if ($fb_id) {
                if ($fb = social_fb_peer::get($fb_id)) {
                    if ($fb['user_id'] && $user = user_peer::get($fb['user_id'])) {
                        user::authorize($fb['user_id']);

                        if (!$raw['birthday'] && empty($fb['birthday']) && $user['age'] == 18 ) {
                            cache::set('users:above18' . $user['id'], 1, cache::TTL_DAY);
                        }

                        if(!$raw['gender'] && empty($fb['gender']) && $user['gender'] == 'm') {
                            cache::set('users:nogender' . $user['id'], 1, cache::TTL_DAY);
                        }

                    } else {
                        $need_link_to_fb = true;
                    }
                } else {
                    $need_link_to_fb = true;
                }

                $u = db_mysql::get_row( 'SELECT * FROM ' . user_peer::TABLE . ' WHERE email = :email AND channel IN (\'flirchi.ru\', \'flirchi.com\', \'FB_application\', \'naij.com\') ORDER BY last_ts DESC LIMIT 1', [':email' => $data['email']], user_peer::CONNECTION );

                if ($need_link_to_fb && !empty($u['id']) && config::get('fb-app')) {
                    if ( !user_peer::get($fb['user_id']) ) {
                        social_fb_peer::insert(array(
                            'fb_id' => $raw['fb_id'],
                            'user_id' => $u['id'],
                            'ts' => $u['ts'],
                            'gender' => $u['gender'],
                            'email' => $u['email'],
                        ));
                    } else {
                        social_fb_peer::save($raw['fb_id'], array('user_id' => $u['id']));
                    }
                    user::authorize($u['id']);
                    $user = $u;
                }

                $domain = strtolower( trim( stristr( $data['email'], '@' ), '@' ) );
                $domain = mail_peer::resolve_alias( $domain );
                if (in_array($domain, array('hotmail.com', 'yahoo.com', 'mail.ru'))) {
                    $activate = false;
                }
            }
        }

        if ( $raw['yandex'] && $yandex_id = $raw['yandex_id'] ) {
            session::set('user_from','ya');
            if ($yandex = social_yandex_peer::get($raw['yandex_id'])) {
                if ($yandex['user_id'] && user_peer::get($yandex['user_id']) && session::get('no-age') != true) {
                    user::authorize($yandex['user_id']);
                    $user = user_peer::get($yandex['user_id']);
                    if (!$raw['birthday'] && empty($yandex['birthday']) && $user['age']==18) {
                        cache::set('users:above18' . $user['id'], 1, cache::TTL_DAY);
                    }
                    if (!$raw['gender'] && empty($yandex['gender']) && $user['gender']=='m') {
                        cache::set('users:nogender' . $user['id'], 1, cache::TTL_DAY);
                    }
                }
            }

            $activate = true;
        }

        if ($raw['vk']) {
            $vk_id = self::vk_id();
            if (!$vk_id) {
                $vk_id = $raw['vk_id'];
            }

            session::set('user_from','vk');
            if (($vk = social_vk_peer::get($vk_id))) {
                if ($vk['user_id'] && ($user = user_peer::get($vk['user_id']))) {
                    $save_params = ['partner' => $data['partner']];
                    if (strpos($user['email'], '@vkontakte.ru') !== false && strpos($raw['email'], '@vkontakte.ru') === false) {
                        $save_params['email'] = $raw['email'];
                    }

                    user_peer::save($vk['user_id'], $save_params);
                    if (config::get('api-mode') == 'vk') {
                        user_peer::activate($vk['user_id']);
                    }

                    user::authorize($vk['user_id']);
                    $user = user_peer::get($vk['user_id']);
                }
            }
        }

        if ($user) {
            if(db_redis::sismember('users:askgeo', $user['id'])) {
                session::set('askgeo', true);
                session::set('askgeo-shown', 0);
            }

            if ($user && !$user['city_id'] && $data['city_id']) {
                user_peer::save($user['id'], array('city_id' => $data['city_id']));
            }

            return $user;
        }

        // After this line start registration process

        if ($data['partner']) {
            $partner = partner_peer::get($data['partner']);
        }

        if (((int)$partner['spamer']) == 1) {
            return;
        }

        if (!$email = helper::email_fix($data['email'])) {
            $error = __('Нужно ввести правильный Email');
            return;
        }

        if ($user = user_peer::get_by('email', $data['email'])) {
            if ($raw['fb'] && $fb_id) {
                social_fb_peer::insert(array(
                    'user_id' => $user['id'],
                    'fb_id' => $fb_id,
                    'ts' => time(),
                    'birthday' => $raw['birthday']
                ));

                user::authorize($user['id']);
                return $user;

            } elseif ($raw['vk'] && $vk_id) {
                user::authorize($user['id']);
                social_vk_peer::save($vk_id, array('user_id' => $user['id']));
                $force = (request::get('referrer') == 'ad_dgift' ? true : false);

                if (config::get('api-mode') == 'vk') {
                    user_peer::activate($user['id']);
                }
                return $user;

            } elseif (helper_mail::is_hotmail($data['email'])) {
                user::authorize($user['id']);
                return $user;
            } elseif ($raw['odnkl']) {
                user::authorize($user['id']);
                return $user;
            }

            if (!$raw['auto_login'] && !$raw['odnkl']) {
                helper_rtg::inc('auth_error_email_taken');
                user::authorize($user['id']);
                return $user;
            }
        }

        if (is_numeric($raw['gender'])) {
            $data['gender'] = $raw['gender'] == 1 ? 'f' : 'm';
        } elseif (is_string($raw['gender'])) {
            $data['gender'] = $raw['gender'] == 'male' ? 'm' : ($raw['gender'] == 'female' ? 'f' : $data['gender']);
        }

        if ($raw['birthday']) {
            $delimiter = '.';
            if (strpos($raw['birthday'], '/')) {
                $delimiter = '/';
            } elseif (strpos($raw['birthday'], '-')) {
                $delimiter = '-';
            }

            //у фб формат m/d/y
            if (!empty($raw['fb'])) {
                list($month, $day, $year) = explode($delimiter, $raw['birthday']);
            } else {
                list($day, $month, $year) = explode($delimiter, $raw['birthday']);
            }

            if (strlen($day) > 2) {
                $d = $year;
                $year = $day;
                $day = $d;
            }

            if (!$year && config::get('api-mode') == 'vk') {
                $year = date('Y', strtotime('-19 Year'));
            }

            if ($year) {
                $age = date('Y') - $year - 1;
                if ($month < idate('m')) {
                    $age += 1;
                } else {
                    if (($month == idate('m')) && (idate('d') > $day)) {
                        $age += 1;
                    }
                }
            }

            $data['age'] = $age;
        }

        $need_change_name = false;

        if (!messages_filter_peer::is_clear_name($data['name'])) {
            $data['name'] = $data['gender'] == 'f' ? __('Безымянная') : __('Безымянный');
            $need_change_name = true;
            helper_rtg::inc('h_reject_name_register');
        }

        if ( user::is_authenticated() && strpos( user::get_data('email'), '@auto.login' ) !== false ) {

            $id = user::id();
            db_redis::srem('users:noname', $id);
            db_redis::srem('users:nogender', $id);
            db_redis::srem('users:above18', $id);

            if (!$data['age']) {
                $data['age'] = 22;
            }

            unset($data['partner']);
            user_peer::save($id, $data);
            helper_rtg::inc('user_auto_login_auth_' . user::get_data('partner'));

        } else {
            $maxmind_data = helper_geo_maxmind::maxmind_geo();

            $maxmind_extended = $maxmind_data['maxmind'];
            $maxmind_data = $maxmind_data['local'];

            $max_city_id = $maxmind_data['city_id']?:0;
            $max_country_id = $maxmind_data['country_id']?:0;
            $max_country_code = $maxmind_data['country_code']?:0;

            if ( $max_city_id ) {
                $city_entity = geo_new_peer::get_by_city($max_city_id);

                if ($city_entity['id'] && empty($city_entity['latitude'])) {
                    if ($city_entity['city_name_ru'] && $city_entity['city_name_ru'] != 'unknown' && ($maxmind_extended['city_name_en'] || $maxmind_extended['city_name_ru']) && !empty($maxmind_extended['latitude']) && !empty($maxmind_extended['longitude'])) {
                        geo_new_peer::city_edit($city_entity['id'], [
                            'latitude' => $maxmind_extended['latitude'],
                            'longitude' => $maxmind_extended['longitude']
                        ]);
                    }
                }
            }

            if ($max_city_id && $max_city_id != $data['city_id']) {
                $data['city_id'] = $city_id = $max_city_id;
            } elseif (!$max_city_id && ($maxmind_extended['city_name_en'] || $maxmind_extended['city_name_ru']) && $max_country_id) {
                $new_city_id = geo_new_peer::city_add(['name_ru' => $maxmind_extended['city_name_ru'], 
                    'name_en' => $maxmind_extended['city_name_en'], 
                    'name_cn' => $maxmind_extended['city_name_cn'], 
                    'name_br' => $maxmind_extended['city_name_br'], 
                    'latitude' => $maxmind_extended['latitude'], 
                    'longitude' => $maxmind_extended['longitude'], 
                    'geoname_id' => $maxmind_extended['city_geoname_id'], 
                    'country_id' => geo_new_peer::country_id_by_code($maxmind_extended['country_code'])]
                );

                if ($new_city_id) {
                    $data['city_id'] = $city_id = $new_city_id;
                }
            }

            if ($raw['yh']) {
                session::set('user_from', 'yh');
            }

            if ($raw['hm']) {
                session::set('user_from', 'hm');
            }

            if ($raw['form']) {
                session::set('user_from', 'form');
            }

            $id = user_peer::insert($data);

            if (!$id) {
                return false;
            }

            context::set('just-user-inserted', true);

            if (!$max_city_id || (!empty($city_entity) && $city_entity['name'] == 'unknown')) {
                $def_country_code = geo_new_peer::country_by_id($geo_config['default_country']['id'])['code'];
                debug('m:' . helper::is_mobile() . ', ' . $def_country_code . ', ' . $id . ' > ' . $max_city_id . ', ' . $max_country_code . ' --- ' . $maxmind_extended['latitude'] . ', ' . $maxmind_extended['longitude'] . ', ' . $maxmind_extended['country_name'] . ', ' . $maxmind_extended['city_name_ru'] . ', ' . $maxmind_extended['city_name_en'], 'debug_maxmind_geo');
            }

            if (strpos($raw['email'], '@auto.login') !== false) {
                if (!$raw['v']) {
                          user_peer::enable_vip($id, 1);
                }
            }

            if (in_array($data['channel'], ['plirtario.com', 'namorico.me'])) {
                user_peer::enable_vip($id, 7);
            }

            if ($_COOKIE['restr1_act'] || ( $data['partner'] && partner_settings_peer::get_setting( $data['partner'], 'enable_pay_funnel', false ) )) {
                cache::set('restrict1_activity_'.$id, true);
                unset($_COOKIE['restr1_act']);
                setcookie('restr1_act', null, -1, '/', config::get('cookie-domain'));
            }

            if ($_COOKIE['restr2_act']) {
                cache::set('restrict2_activity_'.$id, true);
                unset($_COOKIE['restr2_act']);
                setcookie('restr2_act', null, -1, '/', config::get('cookie-domain'));
            }

        }

        context::set('just-registered', true);
        cache::set('update_first_stats_user_' . $id, time(), 60*4);

        if (($inviter_id = session::get('sign_friend')) || ($inviter_id = $_COOKIE['sign_friend'])) {
            $inviter = user_peer::get($inviter_id);
            if (!$inviter['role']) {
                inviters_peer::insert(array('user_id' => $id, 'inviter_id' => $inviter_id, 'ts' => time()));
            }
        }

        if (!$raw['photos'] && !$raw['photo_url'] && helper_mail::is_mail_ru($data['email'])) {
            $nik_name = explode('@', $data['email'])[0];
            $mailru_domain = explode('@', $data['email'])[1];
            $photo_url = 'http://content.foto.my.mail.ru/'.$mailru_domain.'/'.$nik_name.'/_myphoto/h-1.jpg';

            if (($result = http::request($photo_url,null, 'HEAD', [], 10, [ CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
                                                                            CURLOPT_HEADER => true
                                                                          ],CURLINFO_HTTP_CODE))) {
                if (http::$info[CURLINFO_HTTP_CODE] == 200) {
                    $raw['photo_url'] = 'http://content.foto.my.mail.ru/'.$mailru_domain.'/'.$nik_name.'/_myphoto/h-1.jpg';
                    helper_rtg::inc('mm_upl_photo');
                } else {
                    helper_rtg::inc('mm_fail_upl_photo');
                }
            } else {
                helper_rtg::inc('mm_fail_upl_photo');
            }
        }

        if ($raw['photo_url']) {
            user_peer::save($id, array('photo' => 'loading'));
            helper::background_call('user_peer::set_photo_by_url', array($id, $raw['photo_url']));
        } else {
            //user_peer::search_photo($id);
        }

        if ($mm_id) {
            social_mm_peer::insert(array(
                'user_id' => $id,
                'mm_id' => $mm_id,
                'ts' => time(),
                'birthday' => $raw['mm_birthday']
            ));

            if (request::get('friends')) {
                friends::add_mm_friends($mm_id, request::get('friends'));
            }
        }

        if ($fb_id) {
            if ($pc = request::get('pc', session::get('sign-partner-ch'))) {
                switch ($pc) {
                    case 494:
                        helper_rtg::inc('fb-request-reg');
                        break;
                }
            }

            if (!$raw['birthday']) {
                cache::set('users:above18' . $id, 1, cache::TTL_DAY);
            }

            social_fb_peer::insert(array(
                'user_id' => $id,
                'fb_id' => $fb_id,
                'ts' => time(),
                'birthday' => $raw['birthday']
            ));
        }

        if ( $yandex_id ) {

            if (!$raw['birthday']) {
                cache::set('users:above18' . $id, 1, cache::TTL_DAY);
            }

            social_yandex_peer::insert(array(
                'user_id' => $id,
                'yandex_id' => $yandex_id,
                'ts' => time(),
                'birthday' => $raw['birthday']
            ));                   
        }

        if ( $vk_id ) {
            social_vk_peer::insert(array(
                'user_id' => $id,
                'vk_id' => $vk_id,
                'ts' => time(),
                'birthday' => $raw['birthday'],
            ));
            $force = (request::get('referrer') == 'ad_dgift' ? true : false);
        }

        if (session::get('sign-partner-ch')) {
            user_peer::set_data($id, 'partner_channel_id', session::get('sign-partner-ch'));
        }

        $country_id = $geo_config['default_country']['id'];

        if (session::get('user-country-id')) {
            $country_id = session::get('user-country-id');
        } elseif ( !session::get('user-country-id') && $city_id ) {
            $country_id = geo_new_peer::get_by_city($city_id)['country_id'];
        } elseif ( !session::get('user-country-id') && $max_country_id) {
            $country_id = $max_country_id;
        }

        $country_code = strtolower(geo_new_peer::country_by_id($country_id)['code']);
        $partner_data = partner_peer::get($data['partner']);

        if(session::get('language')) {
            $language = session::get('language');
        } else {
            $language = session::get('user-language') ? : i18n::get_language_by_country($country_code);
        }

        if (!$language) {
            $config = config::get('i18n');
            $language = $config['default'];
        }

        Service_User_Geo::update($id, array(
            'country_id' => $country_id,
            'country_code' => $country_code,
            'language' => $language,
        ));

        $user = user_peer::get($id);

        if (helper_mail::is_mail_ru($user['email']) && !helper::is_mobile() && !helper::is_api()) {
            $activate = false;
        }

        if (!helper::is_mobile() && !helper::is_api()) {
            $redirect_to_confirm = true;
        }

        $geo = user_geo_peer::get($id);
        $city = geo_new_peer::get_by_city($user['city_id']);
        //$city_name = geo_peer::city_name($user['city_id']);

        if ($city['code'] != $geo['country_code']) {
            debug('auth: ' . $user['id'] . ', ' . $user['city_id'] . ', city_code: ' . $city['country_code'] . ', geo_code: ' . $geo['country_code'] . ', ' . $user['city_id'] . ':' . geo::city_name($city), 'debug_diff_geo');
        }

        if ($activate) {
            user_peer::activate($id);
        }

        user::authorize($id);

        if ($send_activation) {
            helper_user::send_activation($id);
        }

        track::event('Signup', 'partner-' . ($data['partner'] ? $data['partner'] : 'none'));
        track::event('User', 'Registered');
        track::event('yandex', 'NEWREG');

        if ($need_change_name) {
            session::set('need_change_name', 1);
        }

        if ($raw['photos'] && is_array($raw['photos'])) {
            foreach (array_slice($raw['photos'], 0, 5) as $src) {
                if ($photo = image_storage::upload_by_url($src)) {
                    photo_peer::add($id, $photo);
                    if (!$user['photo']) {
                        user_peer::save($id, array('photo' => $photo));
                    }
                }
            }
        }

        if (!$_COOKIE['auth_bot_strategy']) {
            bot_communicate_peer::init($user['id'], null, 'auth');
            bot_communicate_peer::init($user['id'], null, 'auth');
            if (mt_rand(0, 1)) {
                bot_communicate_peer::init($user['id'], null, 'auth');
            }
            if (mt_rand(0, 1)) {
                bot_communicate_peer::init($user['id'], null, 'auth');
            }
        } elseif ($_COOKIE['auth_bot_strategy'] == 1) {
            bot_communicate_peer::init($user['id'], null, 'auth');
        }

        //wtf ??
        if ($raw['f']) {
            connection_peer::add_friend($id, $raw['f']);
        }

        session::set('show_target_reg_pixel', 1);
        session::set('show_pixels_register', 1);

        if ($redirect_to_confirm) {
            helper_rtg::inc('redirected_to_confirm');
            if (unsubscribe_peer::get($user['email'])) {
                helper_rtg::inc('redirected_to_confirm_unsubscribed');
            }
            response::redirect('/profile/confirm');
        }

        return $user;
    }

    public static function mm_id()
    {
        if (!$_COOKIE['mrc']) {
            return;
        }
        parse_str($_COOKIE['mrc'], $params);

        if ($params['sig']) {
            ksort($params);
            $qp = '';
            foreach ($params as $key => $value) {
                if ($key != 'sig') {
                    $qp .= "$key=$value";
                }
            }
            $sig = md5($qp . config::all()->mailru['secret']);

            if ($sig == $params['sig']) {
                return $params['vid'];
            }
        }
    }

    public static function vk_id()
    {
        if(request::get('vk_id')) {
            return request::get('vk_id');
        }
        $vk_id = request::get_int('viewer_id');
        $vk = config::get('vk');
        $r = md5($vk['id'] . '_' . $vk_id . '_' . $vk['secret']);
        if (request::get('auth_key') == $r) {
            return $vk_id;
        }

        $app_cookie = $_COOKIE['vk_app_'.$vk['id']];
        if (!$app_cookie) {
            return false;
        }

        $valid_keys = array('expire', 'mid', 'secret', 'sid', 'sig');

        $session_data = explode ('&', $app_cookie, 10);
        foreach ($session_data as $pair) {
            list($key, $value) = explode('=', $pair, 2);
            if (empty($key) || empty($value) || !in_array($key, $valid_keys)) {
                continue;
            }
            $session[$key] = $value;
        }

        foreach ($valid_keys as $key) {
            if (!isset($session[$key])) return false;
        }
        ksort($session);

        $sign = '';
        foreach ($session as $key => $value) {
            if ($key != 'sig') {
                $sign .= ($key.'='.$value);
            }
        }

        $sign .= $vk['secret'];
        $sign = md5($sign);
        if ($session['sig'] == $sign && $session['expire'] > time()) {
            return $session['mid'];
        }
        return false;
    }

    public static function fb_id()
    {
        return fb::get_api()->getUser();
    }
}

